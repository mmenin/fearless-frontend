

window.addEventListener('DOMContentLoaded', async () => {

    const conferences_url = 'http://localhost:8000/api/conferences/';
    
    try {

        const response = await fetch(conferences_url);
        
        if (!response.ok) {
            console.error(e);
            alert(`Error: ${e}`, 'warning')
            }
        
        else {

            const data = await response.json();

            const conferenceSelect = document.getElementById('conference');

            for (let conference of data.conferences) {
                const option = document.createElement('option');
                const confUrlDecomposeList = conference.href.split('/')
                option.value = confUrlDecomposeList[confUrlDecomposeList.length-2]   //  what is best practice?
                option.innerHTML = conference.name;
                conferenceSelect.appendChild(option);
                }
            }
    }
    
    catch {
        console.error(e);
        alert(`Error: ${e}`, 'warning')
    }

});

const formTag = document.getElementById('create-presentation-form');
formTag.addEventListener('submit', async (event) => {

    event.preventDefault();

    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));

    const locationUrl = `http://localhost:8000/api/conferences/${ formData.get('conference') }/presentations/`;
    const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
            },
        };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
        formTag.reset();
        const newPresentation = await response.json();
        console.log(newPresentation);
    }
});