

window.addEventListener('DOMContentLoaded', async () => {

    const locations_url = 'http://localhost:8000/api/states/';
    
    try {

        const response = await fetch(locations_url);
        
        if (!response.ok) {
            console.error(e);
            alert(`Error: ${e}`, 'warning')
            }
        
        else {

            const data = await response.json();
            const stateSelect = document.getElementById('state');
            for (let state of data.states) {
                const option = document.createElement('option');
                option.value = state.abbreviation;
                option.innerHTML = state.name;
                stateSelect.appendChild(option);
                }
            }
    }
    
    catch {
        console.error(e);
        alert(`Error: ${e}`, 'warning')
    }

});

const formTag = document.getElementById('create-location-form');
formTag.addEventListener('submit', async (event) => {

    event.preventDefault();

    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));

    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
            },
        };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
        formTag.reset();
        // const newLocation = await response.json();
        // console.log(newLocation);
    }
});