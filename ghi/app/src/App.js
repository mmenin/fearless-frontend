import './App.css';
import AttendeesList from './AttendeesList';
import Nav from './Nav.js';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendanceForm from './AttendConference';

function App(props) {

  if (props.attendees === undefined){
    return null;
  }
  
  return (
    <>
    <Nav />
    <div className="container">
      {/* <AttendeesList attendees={props.attendees} /> */}
      {/* <LocationForm /> */}
      {/* <ConferenceForm /> */}
      <AttendanceForm />
    </div>
  </>
  );
}

export default App;



// if (props.attendees === undefined){
//   return null;
// }
// return (
//   <div>
//     Number of attendees: {props.attendees.length}
//   </div>
// );
