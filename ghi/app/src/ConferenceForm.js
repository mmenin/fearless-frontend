import React from 'react';


class ConferenceForm extends React.Component {


    constructor(props) {
        super(props)
        this.state = {
            name: '',
            description:'',
            starts: '',
            ends: '',
            maxPres: '',
            maxAtt: '',
            location: '',
            locations: []
          };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleStartsChange = this.handleStartsChange.bind(this);
        this.handleEndsChange = this.handleEndsChange.bind(this);
        this.handleMaxPresChange = this.handleMaxPresChange.bind(this);
        this.handleMaxAttChange = this.handleMaxAttChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.max_presentations = data.maxPres;
        data.max_attendees = data.maxAtt;
        delete data.maxPres;
        delete data.maxAtt;
        delete data.locations;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          const newConference = await response.json();
          console.log(newConference);

          const cleared = {
            name: '',
            description:'',
            starts: '',
            ends: '',
            maxPres: '',
            maxAtt: '',
            location: '',
          };
          this.setState(cleared);
        }

    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }
    handleDescriptionChange(event) {
        const value = event.target.value;
        this.setState({description: value})
    }
    handleStartsChange(event) {
        const value = event.target.value;
        this.setState({starts: value})
    }
    handleEndsChange(event) {
        const value = event.target.value;
        this.setState({ends: value})
    }
    handleMaxPresChange(event) {
        const value = event.target.value;
        this.setState({maxPres: value})
    }
    handleMaxAttChange(event) {
        const value = event.target.value;
        this.setState({maxAtt: value})
    }
    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value})
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';
    
        const response = await fetch(url);
    
        if (response.ok) {
          const data = await response.json();
          this.setState({locations: data.locations});
          
        }
      }

    render() {
        return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">


                {/* <h1>Create a new location</h1>
                <form onSubmit={this.handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} placeholder="Name" required
                        type="text" name="name" id="name" className="form-control" value={this.state.name} />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleRoomChange} placeholder="Room count" required
                    type="number" name="room_count" id="room_count" className="form-control" value={this.state.roomCount} />
                    <label htmlFor="room_count">Room count</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleCityChange} placeholder="City"
                    required type="text" name="city" id="city" className="form-control" value={this.state.city} />
                    <label htmlFor="city">City</label>
                </div>
                <div className="mb-3">
                    <select onChange={this.handleStateChange} required name="state"
                     id="state" className="form-select" value={this.state.state}>
                        <option value="">Choose a state</option>
                        {this.state.states?.map(state => {
                            return (
                            <option value={state.abbreviation} key={state.abbreviation}>
                                {state.name}
                            </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form> */}


                <h1>Create a new conference</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleNameChange} placeholder="Name"
                required type="text" id="name" name="name" className="form-control" value={this.state.name} />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <textarea onChange={this.handleDescriptionChange} placeholder="Description" 
                required type="text" id="description" name="description" className="form-control" value={this.state.description} />
                <label htmlFor="name">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleStartsChange} placeholder="Start date"
                required type="date" id="starts" name="starts" className="form-control" value={this.state.starts}/>
                <label htmlFor="starts">Start date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleEndsChange} placeholder="End date"
                required type="date" id="ends" name="ends" className="form-control" value={this.state.ends}/>
                <label htmlFor="ends">End date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleMaxPresChange} placeholder="Max presentations"
                required type="number" name="max_presentations" id="max_presentations" className="form-control" value={this.state.maxPres}/>
                <label htmlFor="max_presentations">Max presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleMaxAttChange} placeholder="Max attendees"
                required type="number" name="max_attendees" id="max_attendees" className="form-control" value={this.state.maxAtt}/>
                <label htmlFor="max_attendees">Max attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleLocationChange} required id="location" name="location" 
                className="form-select" value={this.state.location} key={this.state.location}>
                  <option value="">Choose a location</option>
                  {this.state.locations?.map(location => {
                            return (
                            <option value={location.id} key={location.id}>
                                {location.name}
                            </option>
                            );
                        })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>



            </div>
            </div>
        </div>
        );
    }
    }


export default ConferenceForm;