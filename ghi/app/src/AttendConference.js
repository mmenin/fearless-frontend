import React from 'react';


class AttendanceForm extends React.Component {


    constructor(props) {
        super(props)
        this.state = {
            name: '',
            email: '',
            companyName: '',
            conference: '',
            conferences: []
          };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleConfChange = this.handleConfChange.bind(this);
        // this.handleCompanyChange = this.handleCompanyChange(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        // data.company_name = data.companyName;
        // delete data.companyName;
        delete data.conferences;
        console.log(data);

        const attendeeUrl = 'http://localhost:8000/api/attendees/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {
          const newAttendee = await response.json();
          console.log(newAttendee);

          const cleared = {
            name: '',
            email:'',
            // companyName:'',
            conference: '',
          };
          this.setState(cleared);
        }

    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }
    handleEmailChange(event) {
        const value = event.target.value;
        this.setState({email: value})
    }
    handleCompanyChange(event) {  
      const value = event.target.value;
      this.setState({companyName: value})
    }
    handleConfChange(event) {
        const value = event.target.value;
        this.setState({conference: value})
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';
    
        const response = await fetch(url);
    
        if (response.ok) {
          const data = await response.json();
          this.setState({conferences: data.conferences});
          
        }
      }

    render() {
      let spinnerClasses = 'd-flex justify-content-center mb-3';
      let dropdownClasses = 'form-select d-none';
      if (this.state.conferences.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
      }
        return (
        <div className="row" >
        <div className="col-md-4 ">
          <img src="/logo.svg" alt="conferenceGOlogo" width="90%" className="shadow p-2 mt-4"></img>
        </div>
        <div className="col">
        <div className="row">
            <div className="shadow p-4 mt-4">

              <form id="create-attendee-form">
                <h1 className="card-title">It's Conference Time!</h1>
                <p className="mb-3">
                  Please choose which conference
                  you'd like to attend.
                </p>
                <div className={spinnerClasses} id="loading-conference-spinner"> 
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                <select onChange={this.handleConfChange} required name="conference"
                     id="conference" className={dropdownClasses} value={this.state.conference}>
                        <option value="">Choose a conference</option>
                        {this.state.conferences?.map(conference => {
                            return (
                            <option value={conference.href} key={conference.href}> 
                                {conference.name}
                            </option>
                            );
                        })}
                    </select>
                </div>
                <p className="mb-3">
                  Now, tell us about yourself.
                </p>
                <div className="row">
                  <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={this.handleNameChange} required placeholder="Your full name" type="text" id="name" name="name" className="form-control" />
                        <label htmlFor="name">Your full name</label>
                      </div>
                  </div>
                  <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={this.handleEmailChange} required placeholder="Your email address" type="email" id="email" name="email" className="form-control" />
                        <label htmlFor="email">Your email address</label>
                      </div>
                  </div>
                  </div>
                  <div className="row">
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input required placeholder="Company name" type="text" id="companyName" 
                        name="companyName" className="form-control" />  
                        {/* onChange={this.handleCompanyChange} */}
                        <label htmlFor="companyName">Company name</label>
                      </div>
                  </div>
                  {/* <div className="col"></div> */}
                </div>
                <button className="btn btn-lg btn-primary">I'm going!</button>
              </form>
              <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations! You're all signed up!
              </div>
            </div>
          </div>
        </div>
        </div>
        // </div>
        );
    }
    }

export default AttendanceForm;