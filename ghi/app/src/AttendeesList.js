import './App.css';


function AttendeesList(props) {

      return (
        <>
          <div className="container">
          <table className="table table-striped">
            <thead>
              <tr>
                <th className="col">Name</th>
                <th className="col">Conference</th>
              </tr>
            </thead>
            <tbody>
            {props.attendees?.map(attendee => {
              return (
                <tr key={attendee.href}>
                  <td>{ attendee.name }</td>
                  <td>{ attendee.conference }</td>
                </tr>
              );
            })}
            </tbody>
          </table>
        </div>
      </>
      );
  

}


export default AttendeesList;